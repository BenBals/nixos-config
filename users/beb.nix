{config, pkgs, ...}:
{
  users.extraUsers.beb = {
    isNormalUser = true;
    uid = 1000;
    group = "users";
    extraGroups = [
      "wheel" "disk" "audio" "video" "networkmanager" "systemd-journal" "libvirtd" "docker" "dialout"
    ];
    createHome = true;
    home = "/home/beb";
    shell = pkgs.fish;

    # These are needed for podman to run
    subUidRanges = [
      { count = 65534; startUid = 100001; }
    ];
    subGidRanges = [
      { count = 65534; startGid = 100001; }
    ];
  };
  home-manager.users.beb = import ./beb/home.nix;
}
