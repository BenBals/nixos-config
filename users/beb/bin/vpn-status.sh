#! /run/current-system/sw/bin/bash

if nmcli connection show --active | rg "vpn" > /dev/null; then
    echo $(nmcli connection show --active | rg vpn | cut -d" " -f1)
else
    echo "No VPN"
fi
