{
  home.file.".ssh".source = ./ssh;

  home.file.".profile".text = ''
    export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
  '';

  # Editors
  home.file.".spacemacs".source = ./editors/spacemacs;
  home.file.".vimrc".source = ./editors/vimrc;
  home.file."init.vim" = {
    target = ".config/nvim/init.vim";
    text = "source ~/.vimrc";
  };
  home.file.".doom.d" = {
    source = ./editors/doom.d;
    onChange = ''
      .emacs.d/bin/doom sync 
      .emacs.d/bin/doom upgrade 
      pkill -9 emacs
      emacs-start
    '';
  };

  # Terminal
  home.file."termite" = {
    target = ".config/termite/config";
    source = ./terminal/termite.config;
  };

  home.file.".ghci".text = ''
    :set prompt "\ESC[1;35m\x03BB> \ESC[m"
    :set prompt-cont "\ESC[1;35m > \ESC[m
    :set +s
    :set +t
  '';

  home.file.".bashrc".text = ''
    export PATH=~/dotfiles/bin:$PATH
    alias e='emacsclient --create-frame'
    alias et='emacsclient'
  '';

  home.file.".direnvrc".text = ''
    use_nix() {
      eval "$(lorri direnv)"
      systemctl --user start lorri@$(systemd-escape $(pwd))
    }
  '';

  nixpkgs.config = import ./nixpkgs-config.nix;
  xdg.configFile."nixpkgs/config.nix".source = ./nixpkgs-config.nix;

  # Xstuff
  home.file.".Xmodmap_desktop".source = ./xstuff/Xmodmap_desktop;
  home.file.".Xmodmap_laptop".source = ./xstuff/Xmodmap_laptop;
  home.file."xmonad.hs" = {
   target = ".xmonad/xmonad.hs";
   source = ./xstuff/xmonad.hs;
   onChange = "xmonad --recompile";
  };
  home.file."NamedActions.hs" = {
   target = ".xmonad/lib/NamedActions.hs";
   source = ./xstuff/lib/NamedActions.hs;
  };

  home.file.".npmrc".text = "prefix=~/.npm";

  home.file."registries.conf" = {
    target = ".config/containers/registries.conf";
    text = ''
      [registries.search]
      registries = ['docker.io', 'registry.gitlab.com']
    '';
  };

  home.file."policy.json" = {
    target = ".config/containers/policy.json";
    text = ''
      {
          "default": [
              {
                  "type": "insecureAcceptAnything"
              }
          ],
          "transports":
              {
                  "docker-daemon":
                      {
                          "": [{"type":"insecureAcceptAnything"}]
                      }
              }
      }
    '';
  };

  home.file."marvin.desktop" = {
    target=".local/share/applications/Marivn.desktop";
    text = ''
      [Desktop Entry]
      Name=Marvin
      GenericName=Task Manaer
      Comment=Productivity App
      Exec=~/dotfiles/bin/Marvin
      Type=Application
      Terminal=false
      Categories=Development;TextEditor;Utility;
    '';
  };

  home.file."factorio.desktop" = {
    target = ".local/share/applications/Factorio.desktop";
    text = ''
      [Desktop Entry]
      Name=Factorio
      Exec=/home/beb/.nix-profile/bin/factorio
      Type=Application
      Categories=Game;
    '';
  };

  home.file."xmonad-gnome-nopanel.session" = {
    target = ".config/gnome-session/sessions/xmonad-gnome-nopanel.session";
    text = ''
[GNOME Session]
Name=XMonad/Gnome (without gnome-panel) 
RequiredComponents=gnome-settings-daemon;
RequiredProviders=windowmanager;notifications;
DefaultProvider-windowmanager=xmonad
DefaultProvider-notifications=notification-daemon
'';
  };

  home.file."i3" = {
    target = ".config/i3/config.bak";
    source = ./xstuff/i3config;
  };

  home.file."rofi" = {
    target = ".config/rofi/config.rasi";
    source = ./xstuff/rofi.rasi;
  };

  home.file."taffybar" = {
    target = ".config/taffybar/taffybar.hs";
    source = ./xstuff/taffybar.hs;
  };
  
  home.file."org-notes-latex-templates" = {
    target = ".emacs.d/.local/custom-org-latex-classes";
    source = ./editors/doom.d/latex-templates;
  };
}
