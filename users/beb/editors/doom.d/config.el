;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; refresh' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Ben Bals"
      user-mail-address "benbals@posteo.de")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "IBM Plex Mono" :size 20)
      doom-variable-pitch-font (font-spec :family "IBM Plex Serif" :size 30)
      mixed-pitch-set-height t)

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. These are the defaults.
(setq doom-theme 'modus-operandi
      modus-operandi-theme-rainbow-headings t
      modus-operandi-theme-variable-pitch-headings t
      modus-operandi-theme-scale-headings t)

;; If you intend to use org, it is recommended you change this!
(setq org-directory "~/Dropbox/")

;; roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/Dropbox/roam"))
  (org-roam-file-exclude-regexp (file-truename "orgmode-koenig"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (setq org-id-track-globally t)
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

(map! :leader
  (:prefix ("r" . "roam")
    :desc "Open random node"           "a" #'org-roam-node-random
    :desc "Find node"                  "f" #'org-roam-node-find
    :desc "Find ref"                   "F" #'org-roam-ref-find
    :desc "Show graph"                 "g" #'org-roam-graph
    :desc "Insert node"                "i" #'org-roam-node-insert
    :desc "Capture to node"            "n" #'org-roam-capture
    :desc "Toggle roam buffer"         "r" #'org-roam-buffer-toggle
    :desc "Launch roam buffer"         "R" #'org-roam-buffer-display-dedicated
    :desc "Sync database"              "s" #'org-roam-db-sync))

(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
    :after org-roam ;; or :after org
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

;; Note export
;; From https://www.colinmclear.net/posts/teaching-notes/
(defun cpm/org-export-pdf-notes ()
"Export subtree of notes to PDF file. Note uses a distinctive quote style."
(interactive)
(let ((org-latex-default-quote-environment "quote-b"))
  (org-narrow-to-subtree)
  (save-excursion
    (goto-char (point-min))
    (org-latex-export-to-pdf t t nil nil '(:latex-class "org-notes")))
  (widen)))

(defun cpm/org-export--file-pdf-notes ()
  "Export file notes to PDF file. Note uses a distinctive quote style."
  (interactive)
  (let ((org-latex-default-quote-environment "quote-b"))
    (save-excursion
      (goto-char (point-min))
      (org-latex-export-to-pdf t nil nil nil '(:latex-class "org-notes")))))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("org-notes"
                 "\\documentclass[12pt]{article}
                  [NO-DEFAULT-PACKAGES]
                  [EXTRA]
                  \\input{/home/beb/.emacs.d/.local/custom-org-latex-classes/notes-setup-file.tex}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; Presentation slides (with notes)
(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("beamer-presentation"
               "\\documentclass[presentation]{beamer}
                [NO-DEFAULT-PACKAGES]
                [PACKAGES]
                \\usepackage{pgfpages}
                [EXTRA]
                \\setbeameroption{show notes on second screen=right}
                \\setbeamertemplate{note page}{\\pagecolor{yellow!5}\\insertnote}
                \\input{/home/beb/.emacs.d/.local/custom-org-latex-classes/unl-beamer-preamble.tex}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))


;; Making handouts for slides that don't just look like slides
(with-eval-after-load 'ox-latex
(add-to-list 'org-latex-classes
             '("beamer-handout"
               "\\documentclass[12pt]{article}
                [NO-DEFAULT-PACKAGES]
                [EXTRA]
                \\input{/home/beb/.emacs.d/.local/custom-org-latex-classes/handout-setup-file.tex}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

;; Originally used for exporting notes in reveal.js
;; See
;; https://joonro.github.io/Org-Coursepack/Lectures/04%20Creating%20Content%20for%20Slides%20and%20Handouts.html#speaker-notes

(defun string/starts-with (string prefix)
  "Return t if STRING starts with prefix."
  (and (string-match (rx-to-string `(: bos ,prefix) t) string) t))

(defun my/process-NOTES-blocks (text backend info)
  "Filter NOTES special blocks in export."
  (cond
   ((eq backend 'rst)
    (if (string/starts-with text ".. NOTES::") ""))
   ((eq backend 'html)
    (if (string/starts-with text "<div class=\"NOTES\">") ""))
   ((eq backend 'beamer)
    (let ((text (replace-regexp-in-string "\\\\begin{NOTES}" "\\\\note{" text)))
      (replace-regexp-in-string "\\\\end{NOTES}" "}" text)))
   ))

(eval-after-load 'ox '(add-to-list
                       'org-export-filter-special-block-functions
                       'my/process-NOTES-blocks))

;; Org export to slides w/notes
(defun cpm/org-export-beamer-presentation ()
  (interactive)
  (let ((org-export-exclude-tags '("handout")))
    (save-excursion
      (goto-char (point-min))
      (org-beamer-export-to-pdf nil t nil nil '(:latex-class "beamer-presentation")))))

;; I got the tag based selective export idea from J Kitchin
;; https://kitchingroup.cheme.cmu.edu/blog/2013/12/08/Selectively-exporting-headlines-in-org-mode/
(defun cpm/org-export--file-beamer-presentation ()
  (interactive)
  (let ((org-export-exclude-tags '("handout")))
    (save-excursion
      (goto-char (point-min))
      (org-beamer-export-to-pdf t nil nil nil '(:latex-class "beamer-presentation")))))


;; Org export file to handout
(defun cpm/org-export-beamer-handout ()
"Export subtree content to PDF handout. Handout uses a distinctive quote style."
(interactive)
(let ((org-latex-default-quote-environment "quote-b")
      (org-export-exclude-tags '("slides")))
  (org-narrow-to-subtree)
  (save-excursion
    (goto-char (point-min))
    (org-latex-export-to-pdf t t nil nil '(:latex-class "beamer-handout")))
  (widen)))

(defun cpm/org-export--file-beamer-handout ()
  "Export file content to PDF handout. Handout uses a distinctive quote style."
  (interactive)
  (let ((org-latex-default-quote-environment "quote-b")
        (org-export-exclude-tags '("slides")))
    (save-excursion
      (goto-char (point-min))
      (org-latex-export-to-pdf t nil nil nil '(:latex-class "beamer-handout")))))

;; browse window history
(map! "<C-left>" #'winner-undo
      "<C-right>" #'winner-redo)

;; org-ref
(setq reftex-default-bibliography '("~/Dropbox/bibliography/references.bib"))

;; see org-ref for use of these variables
(setq org-ref-bibliography-notes "~/Dropbox/bibliography/notes.org"
      org-ref-default-bibliography '("~/Dropbox/bibliography/references.bib")
      org-ref-pdf-directory "~/Dropbox/bibliography/bibtex-pdfs/")

(setq org-ref-completion-library 'org-ref-ivy-cite)

;; correctly build bibliography on latex export
;; (setq org-latex-pdf-process (list "latexmk -shell-escape -bibtex -f -pdf %f"))

(use-package ivy-bibtex
  :init
  (setq bibtex-completion-bibliography '("~/Dropbox/bibliography/references.bib")
	bibtex-completion-library-path '("~/Dropbox/bibliography/bibtex-pdfs/")
	bibtex-completion-notes-path "~/Dropbox/bibliography/notes.org"
	bibtex-completion-notes-template-multiple-files "* ${author-or-editor}, ${title}, ${journal}, (${year}) :${=type=}: \n\nSee [[cite:&${=key=}]]\n"

	bibtex-completion-additional-search-fields '(keywords)
	bibtex-completion-display-formats
	'((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
	  (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
	  (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	  (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
	  (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
	bibtex-completion-pdf-open-function
	(lambda (fpath)
	  (call-process "open" nil 0 nil fpath))))

(use-package org-ref
  :ensure nil
  :init
  (require 'bibtex)
  (setq bibtex-autokey-year-length 4
	bibtex-autokey-name-year-separator "-"
	bibtex-autokey-year-title-separator "-"
	bibtex-autokey-titleword-separator "-"
	bibtex-autokey-titlewords 2
	bibtex-autokey-titlewords-stretch 1
	bibtex-autokey-titleword-length 5)
  (define-key bibtex-mode-map (kbd "H-b") 'org-ref-bibtex-hydra/body)
  (define-key org-mode-map (kbd "C-c ]") 'org-ref-insert-link)
  (define-key org-mode-map (kbd "s-[") 'org-ref-insert-link-hydra/body)
  (require 'org-ref-ivy)
  (require 'org-ref-arxiv)
  (require 'org-ref-scopus)
  (require 'org-ref-wos))


(use-package org-ref-ivy
  :ensure nil
  :init (setq org-ref-insert-link-function 'org-ref-insert-link-hydra/body
	      org-ref-insert-cite-function 'org-ref-cite-insert-ivy
	      org-ref-insert-label-function 'org-ref-insert-label-link
	      org-ref-insert-ref-function 'org-ref-insert-ref-link
	      org-ref-cite-onclick-function (lambda (_) (org-ref-citation-hydra/body))))

;; suppress warnings in yasnippet needed for tikzcd snipped
(add-to-list 'warning-suppress-types '(yasnippet backquote-change))

;; If you want to change the style of line numbers, change this to `relative' or
;; `nil' to disable it:
(setq display-line-numbers-type 'relative)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', where Emacs
;;   looks when you load packages with `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.
(defun my-setup-indent (n)
  ;; java/c/c++
  (setq c-basic-offset n)
  ;; web development
  (setq coffee-tab-width n) ; coffeescript
  (setq javascript-indent-level n) ; javascript-mode
  (setq js-indent-level n) ; js-mode
  (setq js2-basic-offset n) ; js2-mode, in latest js2-mode, it's alias of js-indent-level
  (setq web-mode-markup-indent-offset n) ; web-mode, html tag in html file
  (setq web-mode-css-indent-offset n) ; web-mode, css in html file
  (setq web-mode-code-indent-offset n) ; web-mode, js code in html file
  (setq css-indent-offset n) ; css-mode
  (setq typescript-indent-level n) ; typescript
  )
(my-setup-indent 2)

;; commands for dictionary and flycheck
(map! :leader "S d" 'ispell-change-dictionary)
(map! :leader "S c" 'flyspell-buffer)

;; load environment based on direnv
(direnv-mode)

"don't display line numbers in text buffers"
(remove-hook! '(text-mode-hook)
              #'display-line-numbers-mode)

;; org latex and babel
(with-eval-after-load 'org
  (setq org-highlight-latex-and-related '(latex))
  (org-babel-do-load-languages
    'org-babel-load-languages
    '((emacs-lisp . t)
      (gnuplot . t))))

;; use multiple fonts in text mode
(use-package mixed-pitch
  :hook
  ;; If you want it in all text modes:
  (text-mode . mixed-pitch-mode)) ;; or org-mode

(after! org
  "Make LaTeX fragments bigger"
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))

  (add-hook 'org-mode-hook
    (lambda ()
      "Center text"
      (olivetti-mode)
      "Automatically toggle latex fragments"
      (org-fragtog-mode)
      (org-num-mode))))

;; open external files
(use-package! crux
  :config (map! "C-c o" 'crux-open-with))

;; command to see the kill ring
(map! :leader "y" 'helm-show-kill-ring)

;; completion
(after! company
  (setq company-idle-delay 0.2
        company-minimum-prefix-length 2)
  (setq company-show-numbers t)

  (add-hook 'evil-normal-state-entry-hook #'company-abort))

(setq-default history-length 1000)
(setq-default prescient-history-length 1000)

; (use-package! lsp-python-ms
;  :ensure t
;  :hook (python-mode . (lambda ()
;                         (require 'lsp-python-ms)
;                         (lsp)))
;  :init ((setq lsp-python-ms-executable (executable-find "python-language-server"))
;         (setq lsp-pyls-plugins-flake8-max-line-length 100)
;(setq lsp-pyls-plugins-flake8-max-line-length 100)
;         ))

(after! notmuch

(setq notmuch-saved-searches
  '((:name "inbox" :query "tag:inbox" :key "i" :sort-order newest-first)
  (:name "unread" :query "tag:unread" :key "u" :sort-order newest-first)
  (:name "accounts" :query "tag:accounts" :key "c" :sort-order newest-first)
  (:name "archive" :query "tag:archive" :key "r" :sort-order newest-first)
  (:name "arbeit" :query "from:@ecs-gmbh.de" :key "w" :sort-order newest-first)
  (:name "hpi" :query "to:ben.bals@student.hpi.de" :key "h" :sort-order newest-first)
  (:name "flagged" :query "tag:flagged" :key "f" :sort-order newest-first)
  (:name "all" :query "*" :key "a" :sort-order newest-first)))

(define-key notmuch-show-mode-map "F"
  (lambda ()
    "mark message as important"
    (interactive)
    (notmuch-show-tag (list "+flagged" "-inbox"))))

(define-key notmuch-show-mode-map "a"
  (lambda ()
    "toggle archive tag for message"
    (interactive)
    (if (member "archive" (notmuch-show-get-tags))
        (notmuch-show-tag (list "-archive +inbox"))
      (notmuch-show-tag (list "+archive" "-inbox"))))))

;; LanguageTool
(use-package languagetool
  :ensure t
  :defer t
  :commands (languagetool-check
             languagetool-clear-suggestions
             languagetool-correct-at-point
             languagetool-correct-buffer
             languagetool-set-language
             languagetool-server-mode
             languagetool-server-start
             languagetool-server-stop)
  :config
  (setq languagetool-java-arguments '("-Dfile.encoding=UTF-8")
        languagetool-console-command "~/.languagetool/languagetool-commandline.jar"
        languagetool-server-command "~/.languagetool/languagetool-server.jar"))
