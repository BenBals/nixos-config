(TeX-add-style-hook
 "notes-setup-file"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("csquotes" "autostyle") ("xcolor" "svgnames") ("nowidow" "all") ("footmisc" "hang" "flushmargin" "stable" "multiple") ("biblatex" "style=alphabetic") ("geometry" "margin=1.25in") ("tcolorbox" "most")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "titling"
    "bookmark"
    "unicode-math"
    "titletoc"
    "csquotes"
    "fontspec"
    "xcolor"
    "enumitem"
    "fancyhdr"
    "lastpage"
    "float"
    "nowidow"
    "xparse"
    "parskip"
    "sectsty"
    "footmisc"
    "manyfoot"
    "relsize"
    "breakcites"
    "biblatex"
    "geometry"
    "setspace"
    "tcolorbox")
   (TeX-add-symbols
    "oldsection"
    "oldparagraph"
    "oldsubparagraph")
   (LaTeX-add-pagestyles
    "plain")
   (LaTeX-add-xparse-macros
    '("\\RenewDocumentCommand{\\section}{s o m}" "section" "s o m" "Renew"))
   (LaTeX-add-xcolor-definecolors
    "block-gray"))
 :latex)

