{ pkgs, ... }:

{
  programs.mbsync.enable = true;
  programs.astroid.enable = true;

  services.mbsync = {
    enable = true;
    preExec = ''
      ${pkgs.afew}/bin/afew -m --all
    '';
    postExec = ''
      ${pkgs.notmuch}/bin/notmuch new
      ${pkgs.afew}/bin/afew -t --new
    '';
  };
  programs.msmtp.enable = true;
  programs.notmuch = {
    enable = true;
    new.tags = [ "unread" "inbox" "new" ];
  };

  programs.afew = {
    enable = true;

    extraConfig = ''
      [SpamFilter]
      [KillThreadsFilter]
      [ListMailsFilter]
      [ArchiveSentMailsFilter]
      [InboxFilter]

      [Filter.1]
      message = Tag all account emails
      query = to:benbals@posteo.eu or to:benbals+accounts@posteo.de
      tags = +accounts;-inbox

      [Filter.2]
      message = Tag all archive emails
      query = folder:posteo/Archives/2019 or folder:hpi/Archives/2019 or folder:posteo/Archives/2020 or folder:hpi/Archives/2020 or folder:posteo/Archives/2021 or folder:hpi/Archives/2021
      tags = +archive;-inbox

      [MailMover]
      folders = posteo/Inbox hpi/Inbox
      rename = True
      max_age = 15

      # rules
      posteo/Inbox = 'NOT tag:inbox':posteo/Archives/2021
      hpi/Inbox = 'NOT tag:inbox':hpi/Archives/2021
    '';
  };

  accounts.email = {
    accounts.posteo = {
      address = "benbals@posteo.de";
      gpg = {
        key = "F9119EC8FCC56192B5CF53A0BF4F64254BD8C8B5";
        signByDefault = true;
      };
      imap.host = "posteo.de";
      mbsync = {
        enable = true;
        create = "maildir";
      };
      msmtp.enable = true;
      notmuch.enable = true;
      primary = true;
      realName = "Ben Justus Bals";
      signature = {
        text = ''
          Mit besten Wünschen
          Ben Justus Bals
          https://beb.ninja
          PGG-Key: https://beb.ninja/benbals.pub
        '';
        showSignature = "append";
      };
      passwordCommand = "${pkgs.libsForQt5.kwallet}/bin/kwallet-query -f Passwords -r posteo default-wallet";
      smtp = {
        host = "posteo.de";
      };
      userName = "benbals@posteo.de";
    };

    accounts.hpi = {
      address = "ben.bals@student.hpi.de";
      gpg = {
        key = "F9119EC8FCC56192B5CF53A0BF4F64254BD8C8B5";
        signByDefault = true;
      };
      imap.host = "owa.hpi.de";
      mbsync = {
        enable = true;
        create = "maildir";
      };
      msmtp.enable = true;
      notmuch.enable = true;
      primary = false;
      realName = "Ben Justus Bals";
      signature = {
        text = ''
          Mit besten Wünschen
          Ben Justus Bals
          https://beb.ninja
          PGG-Key: https://beb.ninja/benbals.pub
        '';
        showSignature = "append";
      };
      passwordCommand = "${pkgs.libsForQt5.kwallet}/bin/kwallet-query -f Passwords -r hpi default-wallet";
      smtp = {
        host = "owa.hpi.de";
      };
      userName = "ben.bals";
    };

    maildirBasePath = "Mail";
  };

  # home.file.".notmuch-config".source = ~/.config/notmuch/notmuchrc;
}
