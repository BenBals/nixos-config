{ pkgs, config, ... }:

{
  imports = [
    ./overlays.nix
    ./files.nix
    ./programs-services.nix
    ./email.nix

    ./git.nix
    ./emacsclient.nix
    ./fish.nix
    ./default-apps.nix
    ./terminal/alacritty.nix
  ];

  home = {
    stateVersion = "22.05";
    username = "beb";
    homeDirectory = "/home/beb";
  };

  programs.home-manager = {
    enable = true;
    path = "…";
  };
}

