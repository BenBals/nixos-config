{ pkgs, ... }:

{
  home.packages = with pkgs; [
    morgen
    obsidian
    htop
    killall
    # nix-review
    texlive.combined.scheme-full
    ripgrep
    entr
    libsecret
    scrot
    # libsForQt5.vlc # Currently causes build failures
    openresolv
    autorandr
    _1password-gui
    sshpass
    ipe
    gimp
    gimpPlugins.resynthesizer
    acpi
    xclip
    xflux
    pinentry-qt
    patchelf
    gparted
    unar
    fd
    dolphin
    chromium
    dropbox
    file
    guvcview
    gvfs
    hunspellDicts.de-de
    imagemagick
    pandoc
    pavucontrol
    tree
    xournalpp
    yubikey-manager-qt
    yubikey-personalization-gui
    yubioath-desktop
    zoom-us
    zotero
    sdcv
    sqlite # needed for org-roam
    graphviz # needed for org-roam
    xcape
    tealdeer
    poppler_utils
    anki
    reaper
    zoxide
    postman

    # needed for doom emacs
    clang
    coreutils

    okular
    calibre
    signal-desktop
    discord
    slack
    element-desktop

    spotify

    # tor-browser-bundle-bin
    jetbrains.datagrip
    jetbrains.clion
    jetbrains.pycharm-professional
    jetbrains.webstorm

    albert
    appimage-run
    rofi
    bc
    
    # python38Packages.python-language-server
    
    aspell
    aspellDicts.de
    aspellDicts.en
    aspellDicts.en-computers
    aspellDicts.en-science

    inkscape-with-extensions
    recoll

    languagetool

    # fonts
    etBook
    ibm-plex
  ];

  services.gnome-keyring.enable = true;
  services.keynav.enable = false;

  programs.emacs = {
    enable = true;
    package = pkgs.emacsGit;
  };
  services.emacs = {
    enable = true;
    package = pkgs.emacsGit;
  };

  xresources.extraConfig = ''
    xterm*font:     *-fixed-*-*-*-18-*
    Xft.dpi: 160
    Xft.autohint: 0
    Xft.lcdfilter:  lcddefault
    Xft.hintstyle:  hintfull
    Xft.hinting: 1
    Xft.antialias: 1
    Xft.rgba: rgb
  '';

  programs.gpg = {
    enable = true;
    settings = {
      no-auto-key-retrieve = true;
    };
    scdaemonSettings = {
      debug-all = true;
      debug-level = "guru";
      log-file = "/tmp/scd.log";
    };
  };

  services.gpg-agent = {
    enable = true;
    enableScDaemon = true;
    enableSshSupport = true;
    defaultCacheTtl = 300;
    defaultCacheTtlSsh = 300;
    maxCacheTtl = 99999;
    extraConfig = ''
      pinentry-program /home/beb/.nix-profile/bin/pinentry
    '';
  };

  programs.exa = {
    enable = true;
    enableAliases = true;
  };

  services.owncloud-client.enable = true;
  services.nextcloud-client.enable = true;

  services.polybar = {
    package = pkgs.polybar.override {
      iwSupport = true;
      pulseSupport = true;
    };
    enable = true;
    config = ./xstuff/polybar.ini;
    script = "polybar bar &";
  };

  programs.fzf = {
    enable = true;
    defaultCommand = "fd --type f";
  };

  programs.zoxide.enable = true;

  programs.direnv.enable = true;
  programs.direnv.nix-direnv.enable = true;

  # required for morgen 
  nixpkgs.config.permittedInsecurePackages = [
    "electron-15.5.2"
  ];
}
