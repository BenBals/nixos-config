{config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
    restic
  ];

  services.restic = {
    backups = {
      remote = {
        initialize = true;
	timerConfig = {
	  OnCalendar = "00:05";
	  RandomizedDelaySec = "5h";
	};
	repository = "b2:thinkbeb-restic:/";
	s3CredentialsFile = "/etc/nixos/secrets/restic-b2.env";
	passwordFile = "/etc/nixos/secrets/restic-password";
	dynamicFilesFrom = "${pkgs.fd}/bin/fd \"\" /home -E \"cache .cache .cargo .cabal\"";
	pruneOpts = [
	  "--keep-daily 7"
	  "--keep-weekly 5"
	  "--keep-monthly 12"
	  "--keep-yearly 75"
	];
      };
    };
  };
}
