# place this file in /etc/nixos/ and add
#     ./programmer-dvorak.nix
# to /etc/nixos/configuration.nix in `imports`
{config, pkgs, ...}:
{
  console.keyMap = "dvorak-programmer";

  environment.systemPackages = with pkgs; [
    xorg.xmodmap
  ];

  services.xserver = {
    enable = true;
    layout = "us";
    xkbVariant = "dvp";
    xkbOptions = "lv3:ralt_switch";
  };
}
