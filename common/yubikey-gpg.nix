{ config, lib, pkgs, ... }:

{
  programs.ssh.startAgent = false;

  services.pcscd = {
    enable = true;
    plugins = [];
  };

  services.yubikey-agent.enable = true;

  environment.systemPackages = with pkgs; [
    yubikey-personalization
  ];

  services.udev.packages = with pkgs; [
    yubikey-personalization
  ];

  security.polkit.extraConfig = ''
    polkit.addRule(function (action, subject) {
      if (
        (action.id == "org.debian.pcsc-lite.access_pcsc" ||
          action.id == "org.debian.pcsc-lite.access_card") &&
        subject.user == "beb"
      ) {
        return polkit.Result.YES;
      }
    });
  '';
}
