{config, pkgs, ...}: 

{
  imports = [
    ./programmer-dvorak.nix
    ./yubikey-gpg.nix
    ./networking.nix
    ./backup.nix
    ./sound.nix
    ./eos-dev.nix
  ];

  environment.systemPackages = with pkgs; [
    vim
    neovim
    curl
    git
    networkmanager
    alacritty
    zip
    unzip
    noisetorch
    gcc
  ];

  system.autoUpgrade.enable = true;

  services.fwupd.enable = true;

  nix.gc.automatic = true;
  nix.gc.dates = "weekly";
  nix.gc.options = "--delete-older-than 30d";

  nix.package = pkgs.nixUnstable;
  nix.extraOptions = ''
    experimental-features = nix-command flakes
  '';
}
