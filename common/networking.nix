{ config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
    wireguard-tools
    mullvad-vpn
  ];

  services.mullvad-vpn.enable = true;

  networking = {
    # required for mullvad, see https://www.reddit.com/r/NixOS/comments/jutvug/cant_get_mullvad_daemon_to_start_or_get_wireguard/
    iproute2.enable = true;

    wireguard.enable = true;
    hostName = "nixos-beb"; # Define your hostname.
    # wireless.enable = true;  # Enables wireless support via wpa_supplicant.
    networkmanager.enable = true;
    firewall.allowedTCPPorts = [ 3000 ];


    # See https://github.com/NixOS/nixpkgs/issues/113589
    firewall.checkReversePath = "loose";
  };

  services.resolved.enable = true;


}
