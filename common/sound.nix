{config, pkgs, ...}: 

{
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    pulse.enable = true;
    alsa.support32Bit = true;
  };

  environment.systemPackages = with pkgs; [
    pamixer
  ];

  hardware.bluetooth.enable = true;
}
