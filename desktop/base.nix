{config, pkgs, ...}: 

{
  imports = [
    ./virtualization.nix
    ./power.nix
    ./vpn.nix
    ./display.nix
  ];

  environment.systemPackages = with pkgs; [
    emacs
    firefox
    libreoffice
    thunderbird
    vlc
  ];

  programs.steam.enable = true;

  services.printing = {
    enable = true;
    drivers = with pkgs; [
      gutenprint
      epson-201106w
      epson-escpr2
      hplip
    ];
  };

  fonts.fontconfig = {
    enable = true;
    localConf = ''
      <?xml version="1.0"?>
      <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
      <fontconfig>
        <alias>
          <family>Verdana</family>
          <prefer><family>Fira Sans</family></prefer>
          <default><family>fixed</family></default>
        </alias>
      </fontconfig>
    '';
  };

  fonts.fonts = with pkgs; [
    # nerdfonts
    liberation_ttf
    fira
    fira-code
    fira-code-symbols
    hasklig
    emacs-all-the-icons-fonts
  ];
}
