{config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
    # aqemu
    virtmanager
    libguestfs
    vde2
    spice-gtk

    iptables-nftables-compat
    dnsmasq
  ];


  boot.kernelModules = [ "kvm-amd" "kvm-intel" ];

  virtualisation = {
    libvirtd.enable = true;
    docker.enable = true;
    podman.enable = true;
  };
}
