{config, pkgs, ...}:

let 
  nvidia-offload = pkgs.writeShellScriptBin "nvidia-offload" ''
    export __NV_PRIME_RENDER_OFFLOAD=1
    export __NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0
    export __GLX_VENDOR_LIBRARY_NAME=nvidia
    export __VK_LAYER_NV_optimus=NVIDIA_only
    exec -a "$0" "$@"
  '';
in
{
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };

  environment.systemPackages = with pkgs; [
    mesa
    xmonad-log
    nvidia-offload

    # gnome.gnome-tweaks
    # gnomeExtensions.topicons-plus
    # gnomeExtensions.appindicator

    xorg.libxcb
  ];

  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };

  hardware.acpilight.enable = true;
  
  services.xserver = {
    enable = true;
    windowManager = {
      xmonad = {
        enable = true;
        enableContribAndExtras = true;
	extraPackages = haskellPackages: [
          haskellPackages.xmonad-contrib
          haskellPackages.xmonad-extras
          haskellPackages.xmonad
          haskellPackages.dbus
        ];
      };
    };

    displayManager = {
      sddm.enable = true;
      defaultSession = "plasma";
    };

    desktopManager.plasma5.enable = true;

    # displayManager.gdm.enable = true;
    # desktopManager.gnome.enable = true;

    wacom.enable = true;
    libinput.enable = true;
    videoDrivers = [ "vesa" "modesetting" "nvidia" ];
  };
}
