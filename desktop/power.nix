{config, pkgs, ...}:

{
  environment.systemPackages = with pkgs; [
    acpid
    libnotify
    lm_sensors
  ];

  services.logind = {
    lidSwitch = "suspend";
    extraConfig = "
      IdleAction=suspend-then-hibernate
      HandlePowerKey=hibernate
    ";
  };

  services.acpid.enable = true;

  services.upower.enable = true;

  powerManagement = {
    enable = true;
    powertop.enable = true;
  };

  services.thinkfan.enable = true;
}

